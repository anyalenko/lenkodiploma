import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./home";
import { ConfigProvider } from "antd";
import ruRU from "antd/es/locale/ru_RU";
import Calendar from "../calendar/calendar";
import Login from "../main/login";
import ResearchTopics from "../research_topics/researchTopics";
import ProjectsPage from "../projects/projectsPage";
import Activities from "../activities/activities";
import Forum from "../forum/forum";
import Stuff from "../stuff/stuff";
import Seminars from "../seminars/seminars";
import Publications from "../publications/publications";
import Partners from "../partners/partners";
import ContactUs from "../contacts/contacts";


function ModalSwitch() {
  return (
    <ConfigProvider locale={ruRU}>
      <Switch>
        <Route exact path="/" children={<Home />} />
        <Route path="/calendar" children={<Calendar />} />
        <Route path="/login" children={<Login />} />
        <Route path="/researchTopics" children={<ResearchTopics />} />
        <Route path="/projects" children={<ProjectsPage />} />
        <Route path="/forum" children={<Forum />} />
        <Route path="/activities" children={<Activities />} />
        <Route path="/stuff" children={<Stuff />} />
        <Route path="/seminars" children={<Seminars />} />
        <Route path="/publications" children={<Publications />} />
        <Route path="/partners" children={<Partners />} />
        <Route path="/contact_us" children={<ContactUs />} />
      </Switch>
    </ConfigProvider>
  );
}
export default ModalSwitch;
