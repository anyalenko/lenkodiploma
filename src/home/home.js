import React from "react";
import "antd/dist/antd.css";
import { Layout } from "antd";
import MainHeader from "../header/header.js";
import MainFooter from "../footer/footer.js";
import MainInformation from "../main/mainInf.js";
import LeftSider from "../main/leftSider.js";
import RightSider from "../main/rightSider.js";
import SmallCalendar from "../main/smallCalendar.js";
import LastNews from "../main/lastNews.js";
import ForumImage from "../main/forumImage.js";
import Partners from "../main/partners.js";
import Contacts from "../main/contacts.js";
import PublicationsImg from "../main/publicationsImg.js";


const { Header, Content, Footer, Sider } = Layout;

function Home() {
  return (
    <Layout className='myLayout'>
      <Header>
        <MainHeader />
      </Header>
      <Layout>
        <Sider>
          <LeftSider />
        </Sider>
        <Content>
          <MainInformation />
          <LastNews />
          <div className='oneRow'>
            <SmallCalendar />
            <ForumImage />
            <PublicationsImg />
          </div>
          <Partners />
          <Contacts />
        </Content>
        <Sider>
          <RightSider />
        </Sider>
      </Layout>
      <Footer>
        <MainFooter />
      </Footer>
    </Layout>
  );
}

export default Home;
