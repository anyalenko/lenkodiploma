import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import ModalSwitch from "./switch";

function App() {
  return (
    <Router>
      <ModalSwitch />
    </Router>
  );
}

export default App;
