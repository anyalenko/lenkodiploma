import { combineReducers } from 'redux'
import user from './user'
import visibilityFilter from './visibilityFilter'

const rootReducer = combineReducers({
  user,
  visibilityFilter
})

export default rootReducer
