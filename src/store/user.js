import {
  ADD_USER,
  REMOVE_USER
} from './actions'

const initialState = {
  user: null
};

export default function user(state = initialState, action) {
  switch (action.type) {
    case ADD_USER:
      return Object.assign({}, state, {
        user: action.user
      })

    case REMOVE_USER:
      return Object.assign({}, state, {
        user: null
      })

    default:
      return state
  }
}
