import React from "react";
import { Link } from "react-router-dom";


function Publications(){

  return (
    <div className="navigation">
      <div className="navigationText">
        <p className="text">Последние публикации</p>
      </div>
      <ul>
        <Link to="/publication1">
          <li>Публикация 1</li>
        </Link>
        <Link to="/publication2">
          <li>Публикация 2</li>
        </Link>
        <Link to="/publication3">
          <li>Публикация 3</li>
        </Link>
        <Link to="/publication4">
          <li>Публикация 4</li>
        </Link>
      </ul>
    </div>
  );
}

export default Publications;