import React from "react";
import { useHistory, Link } from "react-router-dom";
import Publications from '../images/publications.png';

function PublicationsImg(){

const history = useHistory();

const handlePublicationsClick = e => {
    e.preventDefault();
    history.push('/publications');
}

    return(
        <div className='forum'>
            <Link to = '/publications'>
                <h1 className="text">Публикации</h1>
            </Link>
            <img src = {Publications} alt = 'Publications' className='forumImg publicationImg' onClick = {handlePublicationsClick} />
        </div>
    );
}

export default PublicationsImg;