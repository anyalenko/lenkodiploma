import React from "react";
import Browse from "../main/browse";
import { useHistory } from "react-router-dom";


function LeftSider() {

const history = useHistory();

  const handlerClick = e => {
    e.preventDefault();
    history.push('/login');
  };

  return (
    <>
    <button className='ant-btn enterButton' onClick = {handlerClick}>Войти</button>
      <Browse />
    </>
  );
}

export default LeftSider;
