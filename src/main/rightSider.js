import React from "react";
import WorkTopics from "../main/topics";
import Projects from "../main/projects";
import Publications from "../main/publications";
import Staff from "../main/staff";

function RightSider() {
  return (
    <div>
      <WorkTopics />
      <Projects />
      <Staff />
      <Publications />
    </div>
  );
}

export default RightSider;
