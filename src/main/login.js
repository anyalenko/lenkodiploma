import React, { useState } from "react";
import "antd/dist/antd.css";
import { Form, Input, Button } from "antd";
import { useHistory } from "react-router-dom";
import { useCookies } from 'react-cookie';
import UserApi from '../api/UserApi';


function Login(props) {
  const { getFieldDecorator } = props.form;
  const history = useHistory();
  const [cookies, setToken] = useCookies(['token']);

  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');

  const loginChange = e => {
    e.preventDefault();
    setLogin(e.target.value);
  };

  const passwordChange = e => {
    e.preventDefault();
    setPassword(e.target.value);
  };


  const handleSubmit = e => {
    e.preventDefault();
    props.form.validateFields((err, values) => {
      if (!err) {
        const usersData = {
          email: login,
          password: password
        };

        UserApi.login(usersData)
          .then(response => {
            let token = response.data.access_token;
            setToken('userToken', token);
            UserApi.setTokenToHeader(token);
            UserApi.getProfile();
          })
      }
    });
  };

  return (
    <Form layout="vertical" onSubmit={handleSubmit} className="loginForm">
      <Form.Item label="Логин:">
        {getFieldDecorator("login", {
          rules: [
            {
              required: true,
              message: "Введите логин"
            }
          ]
        })(<Input onChange = {loginChange} />)}
      </Form.Item>

      <Form.Item label="Пароль:">
        {getFieldDecorator("password", {
          rules: [
            {
              required: true,
              message: "Введите пароль"
            },
            {
              required: true,
              message: "Пароль не подходит"
            }
          ]
        })(<Input type="password" onChange = {passwordChange} />)}
      </Form.Item>

      <Form.Item> 
          <Button type="primary" htmlType="submit">
            <span>Войти</span>
          </Button>
      </Form.Item>
    </Form>
  );
}

const WrappedLogin = Form.create({ name: "login" })(Login);

export default WrappedLogin;
