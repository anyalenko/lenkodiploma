import React from "react";
import { useHistory, Link } from "react-router-dom";
import Forum from '../images/forum.png';

function ForumImage(){

const history = useHistory();

const handleImageClick = e => {
    e.preventDefault();
    history.push('/forum');
}

    return(
        <div className='forum'>
            <Link to= '/forum'>
                <h1 className="text" id = 'forumText'>Форум</h1>
            </Link>
            <img src = {Forum} alt = 'forum' className='forumImg' onClick = {handleImageClick} />
        </div>
    );
}

export default ForumImage;