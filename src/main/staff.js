import React from "react";
import 'antd/dist/antd.css';
import { Statistic, Row, Col, Button } from 'antd';
import { useHistory } from "react-router-dom";


function Staff() {

  const history = useHistory();

  const handleClick = e => {
    e.preventDefault();
    history.push('/stuff');
  };

  return (
    <div className="rightSider">
        <Row gutter={16}>
          <Col span={12}>
            <Statistic title="Количество сотрудников" value={123}  />
            <Button style={{ marginTop: 16 }} type="primary" onClick={handleClick}>Подробнее</Button>
          </Col>
        </Row>
    </div>
  );
}

export default Staff;
