import React from "react";
import { Link } from "react-router-dom";

function Browse() {
  return (
    <div className="navigation">
      <div className="navigationText">
        <strong className="navText">Навигация</strong>
      </div>
      <ul>
      <Link to="/researchTopics">
          <li>Темы <br />исследований</li>
        </Link>
        <Link to="/projects">
          <li>Проекты</li>
        </Link>
        <Link to="/forum">
          <li>Форум</li>
        </Link>
        <Link to="/calendar">
          <li>Календарь</li>
        </Link>
        <Link to="/activities">
          <li>Мероприятия</li>
        </Link>
        <Link to="/stuff">
          <li>Сотрудники</li>
        </Link>
        <Link to="/seminars">
          <li>Семинары</li>
        </Link>
        <Link to="/publications">
          <li>Публикации</li>
        </Link>
        <Link to="/partners">
          <li>Партнёры</li>
        </Link>
        <Link to="/contact_us">
          <li>Связь</li>
        </Link>
      </ul>
    </div>
  );
}

export default Browse;
