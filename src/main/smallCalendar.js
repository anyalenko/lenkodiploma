import React from "react";
import { useHistory } from "react-router-dom";
import "antd/dist/antd.css";
import { Calendar } from "antd";

function SmallCalendar() {
  let history = useHistory();

  function onPanelChange(value, mode) {
    console.log(value, mode);
    history.push("/calendar");
  }

  const handleSelect = () => {
    history.push("/calendar");
  };

  return (
    <div className="calendar">
      <Calendar
        fullscreen={false}
        onPanelChange={onPanelChange}
        onSelect={handleSelect}
        onChange={handleSelect}
      />
    </div>
  );
}

export default SmallCalendar;
