import React from "react";
import "antd/dist/antd.css";
import { Collapse } from "antd";
//import axios from 'axios';

const { Panel } = Collapse;

const text = `Список тем, в рамках которой выполняются и выполнялись исследования. `;

function WorkTopics() {

  // axios.get("http://localhost:3000").then(resp => {
  //     const data = resp.data;
  //     const textForFirstTheme = data.firstText;
  // });

  return (
    <div className='rightSider'>
    <p className="text">Темы исследований</p>
      <Collapse accordion>
        <Panel header="Тема 1" key="1">
          <p>{text}</p>
        </Panel>
        <Panel header="Тема 2" key="2">
          <p>{text}</p>
        </Panel>
        <Panel header="Тема 3" key="3">
          <p>{text}</p>
        </Panel>
      </Collapse>
    </div>
  );
}

export default WorkTopics;
