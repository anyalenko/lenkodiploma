import React from "react";
import { Link } from "react-router-dom";


function Projects(){

  return (
    <div className="navigation rightSider">
      <div className="navigationText">
        <p className="text">Популярные проекты</p>
      </div>
      <ul>
        <Link to="/projects">
          <li>Проект 1</li>
        </Link>
        <Link to="/projects">
          <li>Проект 2</li>
        </Link>
        <Link to="/projects">
          <li>Проект 3</li>
        </Link>
        <Link to="/projects">
          <li>Проект 4</li>
        </Link>
      </ul>
    </div>
  );
}

export default Projects;