import axios from 'axios'

class Api {
  constructor () {
    this.axios = axios.create({
      baseURL: 'http://localhost:81/api',
      headers: {}
    })
  }
}

export default Api
