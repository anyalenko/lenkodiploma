import Api from './Api'

class UserApi extends Api {
  login (data) {
    return this.axios.post('/auth/login', data)
  }
  getProfile () {
    return this.axios.get('/user/profile')
  }

  setTokenToHeader (token) {
    if (token) {
      this.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    } else {
      delete this.axios.defaults.headers.common['Authorization'];
    }
  }
}

export default new UserApi()
