import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./home/project";
import "./styles/main.less";
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reactStore from './store/index';
import { CookiesProvider } from 'react-cookie';

const store = createStore(reactStore);

ReactDOM.render(
  <CookiesProvider>
    <Provider store={store}>
      <App />
    </Provider>
  </CookiesProvider>,
  document.getElementById("root")
);
