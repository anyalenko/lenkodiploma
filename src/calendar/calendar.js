import React from "react";
import 'antd/dist/antd.css';
import { PageHeader, Descriptions } from 'antd';
import { BackTop } from 'antd';

function Calendar() {
  return (
  <>
    <div style={{ backgroundColor: '#F5F5F5', padding: 24 }} className = 'header'>
            <PageHeader
                ghost={false}
                onBack={() => window.history.back()}
                title="Календарь"
                //subTitle="This is a subtitle"
                /*extra={[
                    <Button key="3">Operation</Button>,
                    <Button key="2">Operation</Button>,
                    <Button key="1" type="primary">Primary</Button>,
                ]} */
            >
                <Descriptions size="small" column={3}>
                    <Descriptions.Item label="Информация">Информация</Descriptions.Item>
                    <Descriptions.Item label="Информация">Информация</Descriptions.Item>
                    <Descriptions.Item label="Информация">Информация</Descriptions.Item>
                    <Descriptions.Item label="Пометки">Пометки</Descriptions.Item>
                </Descriptions>
            </PageHeader>
            <BackTop /> 
    </div>
  </>
  );
}
export default Calendar;
