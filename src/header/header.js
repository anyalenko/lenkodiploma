import React from "react";

function MainHeader() {
  return (
    <>
      <p id="headerText">Научная лаборатория по компьютерной арифметике</p>
    </>
  );
}

export default MainHeader;
